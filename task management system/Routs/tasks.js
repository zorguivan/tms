var express = require('express');
var tasksRouter = express.Router();
var Task = require('../models/task');


tasksRouter.route('/task')
    .post(function (req, res) {
        var task = new Task();
        task.title = req.body.title;
        task.description = req.body.description;
        task.estimate = req.body.estimate;
        task.priority = req.body.priority;
        task.status = req.body.status;
        task.done = req.body.done;
        task.deadline = req.body.deadline;

        task.save(function (err) {
            if (err)
                res.send(err);
            res.send(200);
        });
    });

tasksRouter.route('/task/:id')
    .put(function (req, res) {
        Task.update({ _id: req.params.id }, {
            $set: {
                title: req.body.title,
                description: req.body.description,
                estimate: req.body.estimate,
                priority: req.body.priority,
                status: req.body.status,
                done: req.body.done,
                deadline: req.body.deadline
            }
        }, function (err) {
            if (err)
                res.send(err);
            res.send(200);
        });

        console.log(req);
        console.log(res);
    });

tasksRouter.route('/task/:id')
    .delete(function (req, res) {
        console.log(req.params.id)
        Task.remove({ _id: req.params.id }, function (err, ress) {
            if (ress) console.log(ress)
            if (!err) console.log('++++++++++++')
            if (err) {
                console.log(err)
                res.send(err)
            }

            res.send(200);
        });
    });

tasksRouter.route('/tasks')
    .get(function (req, res) {
        Task.find({}, function (err, docs) {
            res.send(docs)
        });
    });

tasksRouter.route('/tasks/:id')
    .get(function (req, res) {
        Task.findOne({ _id: req.params.id }, function (err, docs) {
            res.send(docs);
        })
    })

module.exports = tasksRouter;
