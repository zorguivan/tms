var express = require('express');
var invoiceRouter = express.Router();
var Invoices = require('../models/invoice');
var Client = require('../models/client');
var template = require('../template');
var fs = require('fs');
var pdf = require('html-pdf');
var pdfOptions = {
    format: "A4", 
    orientation: "portrait",
    border: {
        top: "1.25cm" ,
        right: "1.25cm",
        left: "1.25cm",
        bottom: "1.25cm"
    }
};


invoiceRouter.route('/invoice')
    .post(function (req, ress) {
        var client;
        var invoice;
        Client.find({'_id' : req.body.client}, function (err, client) {

            Invoices.find({}, function (err, invoice) {
                pdf.create(template(), pdfOptions).toFile('output.pdf', function(err, res) {
                    if (err) return console.log(err);

                    console.log(res);
                    ress.send('ok')
                  });
            });

        });
    });

module.exports = invoiceRouter;