var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ClientSchema = new Schema({
    clientName: String,
    clientEmail: String,
    clientCompany: String,
    clientLocation: String,
    clientPostalCode: String,
    clientAccount: String,
    clientSortCode: String
});

module.exports = mongoose.model('Client', ClientSchema);