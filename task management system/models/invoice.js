var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var InvoicesSchema = new Schema({
    invoiceNumber: Number,
    invoiceOwner: String,
    invoiceUnitPrice: Number,
    invoiceBankName: String,
    invoiceBankAccount: String,
    invoiceBankSort: String
});

module.exports = mongoose.model('Invoices', InvoicesSchema);