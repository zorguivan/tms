var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TaskSchema = new Schema({
    title: String,
    description: String,
    estimate: Number,
    priority: String,
    status: String,
    done: Boolean,
    deadline: String
});

module.exports = mongoose.model('Task', TaskSchema);