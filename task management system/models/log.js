var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var LogSchema = new Schema({
    taskId: String,
    startTime: Number,
    endTime: Number,
    summary: String,
    totalTime: Number
});

module.exports = mongoose.model('Log', LogSchema);